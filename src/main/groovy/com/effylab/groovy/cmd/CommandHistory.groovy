package com.effylab.groovy.cmd;

import groovy.transform.PackageScope;
import groovy.transform.CompileStatic;

@PackageScope
@CompileStatic
class CommandHistory {
    private List<Command> list;
    int maxSize;

    CommandHistory(int maxSize = 100) {
        this.list = new ArrayList<>(maxSize);
        this.maxSize = maxSize;
    }

    int getSize() {
        return list.size();
    }

    void addCommand(Command element) {
        while (list.size() >= maxSize) {
            list.remove(0);
        }
        list.add(element);
    }

    Command getCommand(int index) {
        if ((index < 0) || (index > (list.size() - 1))) {
            return null;
        }
        return list.get(index);
    }

    Command getLastCommand() {
        if (list.isEmpty()) {
            return null;
        }
        return list.get(list.size() - 1);
    }

    List<Command> getElements() {
        return list;
    }
}