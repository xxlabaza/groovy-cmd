package com.effylab.groovy.cmd;

import com.effylab.groovy.cmd.exception.IllegalMethodSignatureException;
import com.effylab.groovy.cmd.exception.MethodReturnTypeException;
import com.effylab.groovy.cmd.exception.MethodArgumentsNumberException;
import com.effylab.groovy.cmd.exception.MethodArgumentTypeException;

import groovy.transform.PackageScope;
import groovy.transform.CompileStatic;

import java.lang.reflect.Method;

@PackageScope
@CompileStatic
class CommandManager {
    
    private Map<String, Command> commands;

    CommandManager(Object methodObject) {
        Command.methodObject = methodObject;
        commands = new HashMap<>();
    }

    void addCommand(Method method) {
        if (!method.returnType.name.equals('boolean')) {
            throw new MethodReturnTypeException(method.name, method.returnType.name);
        } else if (!(method.parameterTypes.length in [1, 2])) {
            throw new MethodArgumentsNumberException(method.name, method.parameterTypes.length);
        } else if ((method.parameterTypes.length == 1) && !method.parameterTypes[0].name.equals(String[].class.name)) {
            throw new MethodArgumentTypeException(method.name, method.parameterTypes[0].name);
        } else if (method.parameterTypes.length == 2) {
            if (!method.parameterTypes[0].name.equals(Options.class.name)) {
                throw new MethodArgumentTypeException(method.name, method.parameterTypes[0].name, method.parameterTypes[1].name);
            } else if (!method.parameterTypes[1].name.equals(String[].class.name)) {
                throw new MethodArgumentTypeException(method.name, method.parameterTypes[0].name, method.parameterTypes[1].name);
            }
        }

        Description description = method.getAnnotation(Description);
        Command command = new Command(
            name:               method.name.substring(3),
            briefDescription:   (description ? description.brief() : 'No description'),
            fullDescription:    (description ? description.full() : 'No description'),
            method:             method,
            arguments:          new String[0]
        );
        if (description && description.opts()) {
            List<Option> options = new ArrayList<>();
            for (Opt opt in description.opts()) {
                options.add(new Option(opt));
            }
            command.options = (Option[]) options.toArray(new Option[options.size()]);
        }
        commands.put(command.name, command);
    }

    Command getCommand(String commandLine) {
        if (!commandLine) {
            return null;
        }
        String[] args = commandLine.trim().split(/\s+(?=([^"']*("|')[^"']*("|'))*[^"']*$)/);
        return getCommand(args);
    }

    Command getCommand(String[] args) {
        if (!args) {
            return null;
        }
        if (!commands.containsKey(args[0])) {
            return null;
        }
        Command command = commands.get(args[0]).clone();
        if (args.length > 1) {
            command.parseArguments(args[1..-1].toArray(new String[args.length - 1]));
        }
        return command;
    }

    List<Command> getCommandsList() {
        return new ArrayList<>(commands.values());
    }
}