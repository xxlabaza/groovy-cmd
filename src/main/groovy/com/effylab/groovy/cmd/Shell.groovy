package com.effylab.groovy.cmd;

import groovy.transform.CompileStatic;

/**
 * A helper class for execution the shell commands.
 * 
 * @see     com.effylab.groovy.Shell.Response
 * 
 * @author  Artem Labazin
 * @version 1.0 11 Feb 2014
 */
@CompileStatic
class Shell {
    
    /**
     * Executes a shell command and returns its response.
     * 
     * @param command   a command string
     * 
     * @return          shell response after executing a command
     */
    static Response execute(String command) {
        StringBuffer out = new StringBuffer();
        StringBuffer err = new StringBuffer();
        Process process = command.execute();
        process.consumeProcessOutput(out, err);
        process.waitForOrKill(1000);
        Response response = new Response(command: command);
        if (err.length() > 0) {
            response.hasError = true;
            response.out = err.toString();
        } else {
            response.out = out.toString();
        }
        return response;
    }

    /**
     * A helper class for representing shell response
     * 
     * @see     com.effylab.groovy.Shell.Response
     * 
     * @author  Artem Labazin
     * @version 1.0 11 Feb 2014
     */
    static class Response {
        
        /** Command string, executed in shell */
        String command;
        
        /** Flag for detecting is error or not execution */
        boolean hasError;
        
        /** Shell output */
        String out;
    }
}