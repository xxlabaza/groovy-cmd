package com.effylab.groovy.cmd;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Describes method of class which extends {@link com.effylab.groovy.cmd.Cmd} class.
 * 
 * @see     com.effylab.groovy.cmd.Cmd
 * 
 * @author  Artem Labazin
 * @version 1.0 11 Feb 2014
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Description {
    
    /**
     * Short description of method. It will be shown in common 'help' output.
     */
    String brief() default 'No description';
    
    /**
     * Full method's description. It will be shown in specific 'help' output, like:
     * <pre>
     *     $> help command
     *     command full description
     * </pre>
     */
    String full() default 'No description';
    
    /**
     * Array opt @Opt annotations for describing command's options.
     * 
     * @see     com.effylab.groovy.cmd.Opt
     */
    Opt[] opts() default [];
}