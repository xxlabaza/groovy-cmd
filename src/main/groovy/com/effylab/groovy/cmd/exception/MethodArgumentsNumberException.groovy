package com.effylab.groovy.cmd.exception;

class MethodArgumentsNumberException extends IllegalMethodSignatureException {

	MethodArgumentsNumberException() {
		super('Method has wrong number of arguments.');
	}

	MethodArgumentsNumberException(String methodName, int argsCount) {
		super("Method '${methodName}(...)' has wrong number of arguments - '${argsCount}'.");
	}

	MethodArgumentsNumberException(String message) {
		super(message);
	}

	MethodArgumentsNumberException(String message, Throwable cause) {
		super(message, cause);
	}

	MethodArgumentsNumberException(Throwable cause) {
		super(cause);
	}
}