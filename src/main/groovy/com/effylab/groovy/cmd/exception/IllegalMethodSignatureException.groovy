package com.effylab.groovy.cmd.exception;

class IllegalMethodSignatureException extends Exception {

	IllegalMethodSignatureException() {
		super('Illegal method signature');
	}

	IllegalMethodSignatureException(String message) {
		super(message);
	}

	IllegalMethodSignatureException(String message, Throwable cause) {
		super(message, cause);
	}

	IllegalMethodSignatureException(Throwable cause) {
		super(cause);
	}
}