package com.effylab.groovy.cmd.exception;

class MethodArgumentTypeException extends IllegalMethodSignatureException {

	MethodArgumentTypeException() {
		super('Method has wrong argument type. It must have or (String[]) or (Options, String[]) argument types.');
	}

	MethodArgumentTypeException(String methodName, String... argumentTypeNames) {
		super("Method '${methodName}(${argumentTypeNames.join(', ')})' has wrong signature. It must be like '${methodName}(String[])' or '${methodName}(Options, String[])'.");
	}

	MethodArgumentTypeException(String message) {
		super(message);
	}

	MethodArgumentTypeException(String message, Throwable cause) {
		super(message, cause);
	}

	MethodArgumentTypeException(Throwable cause) {
		super(cause);
	}
}