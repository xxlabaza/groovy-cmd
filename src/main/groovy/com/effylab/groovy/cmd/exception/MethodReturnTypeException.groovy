package com.effylab.groovy.cmd.exception;

class MethodReturnTypeException extends IllegalMethodSignatureException {

	MethodReturnTypeException() {
		super('Method has wrong return type. Return type must be boolean.');
	}

	MethodReturnTypeException(String methodName, String returnTypeName) {
		super("Method '${returnTypeName} ${methodName}(...)' has wrong return type. Return type must be boolean.");
	}

	MethodReturnTypeException(String message) {
		super(message);
	}

	MethodReturnTypeException(String message, Throwable cause) {
		super(message, cause);
	}

	MethodReturnTypeException(Throwable cause) {
		super(cause);
	}
}