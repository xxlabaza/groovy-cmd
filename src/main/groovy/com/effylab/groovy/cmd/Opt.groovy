package com.effylab.groovy.cmd;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation for describing command's options.
 * 
 * This information uses in 'help' command.
 * 
 * @see     com.effylab.groovy.cmd.Description
 * 
 * @author  Artem Labazin
 * @version 1.2.2 06 Mar 2014
 */
@Target([])
@Retention(RetentionPolicy.RUNTIME)
public @interface Opt {

    /**
     * Name of command's option. It's a mandatory setting. 
     * Optionaly, you can use '|' separator for setting short and long name, like this:
     * <pre>
     *     name='n|network'
     * </pre>
     * In CLI, Cmd parser automaticaly recognises '-' and '--' prefixes for short and long names.
     * Option above has '-n' and '--network' names accordingly.
     */
    String name();

    /**
     * Help string, which describes option's purpose and it has shown in help info.
     * It's am optional setting, which has 'No description' value by default.
     */
    String help() default 'No description';

    /**
     * The setting indicates is option must be setted necessarily or not. 
     * It's an optional setting, which has 'false' value by default.
     */
    boolean required() default false;

    /**
     * The setting describes option as flag or not.
     * If it set in 'true' value, Cmd parser tries to determine the next argument as its value. 
     * It's an optional setting, which has 'false' value by default.
     */
    boolean flag() default false;

    /**
     * A value for option by default.
     * It's an optional setting, which has empty string as default value.
     */
    String byDefault() default '';
}