package com.effylab.groovy.cmd;

import java.util.regex.Matcher;
import java.lang.reflect.Method;

import groovy.transform.PackageScope;
import groovy.transform.CompileStatic;

@PackageScope
@CompileStatic
class Command {

    String name;
    String briefDescription;
    String fullDescription;
    Method method;
    Option[] options;
    String[] arguments;
    private Properties defaults;

    static Object methodObject;

    Command(Map map) {
        this.name = (String) map.get('name');
        this.briefDescription = (String) map.get('briefDescription');
        this.fullDescription = (String) map.get('fullDescription');
        this.method = (Method) map.get('method');
        this.options = (Option[]) map.get('options');
        this.arguments = (String[]) map.get('arguments');

        defaults = new Properties();
        for (Option option in options) {
            if (option.shortName) {
                defaults.setProperty(option.shortName, option.byDefault);
            }
            if (option.longName) {
                defaults.setProperty(option.longName, option.byDefault);
            }
        }
    }

    Command clone() {
        return new Command(
            name:               name,
            briefDescription:   briefDescription,
            fullDescription:    fullDescription,
            arguments:          new String[0],
            method:             method,
            options:            options,
            defaults:           defaults
        );
    }

    boolean invoke() {
        if (options) {
            Properties properties = new Properties(defaults);
            List<String> argumentsList = new ArrayList<>(Arrays.asList(arguments));
            for (Option option in options) {
                boolean isSetted = false;
                for (int i = 0; i < argumentsList.size(); i++) {
                    String arg = argumentsList.get(i);
                    if (arg =~ option.namePattern) {
                        Object optValue = null;
                        if (option.isFlag) {
                            optValue = true;
                        } else {
                            int nextIndex = (i + 1);
                            String value = argumentsList.get(nextIndex);
                            argumentsList.remove(nextIndex);
                            optValue = value;
                        }
                        if (option.shortName) {
                            properties.put(option.shortName, optValue);
                        }
                        if (option.longName) {
                            properties.put(option.longName, optValue);
                        }
                        argumentsList.remove(i);
                        isSetted = true;
                        break;
                    }
                }
                if (!isSetted && option.isRequired) {
                    StringBuilder sb = new StringBuilder();
                    if (option.shortName) {
                        sb.append(option.shortName);
                        if (option.longName) {
                            sb.append('|');
                            sb.append(option.longName);
                        }
                    } else {
                        sb.append(option.longName);
                    }
                    println("ERROR:\tOption '${sb.toString()}' is required");
                    return false;
                }
            }
            Object[] args = new Object[2];
            args[0] = new Options(properties);
            args[1] = argumentsList.toArray(new String[argumentsList.size()]);
            return method.invoke(methodObject, args);
        } else {
            Object[] args = new Object[1];
            args[0] = arguments;
            return method.invoke(methodObject, args);
        }
    }

    void parseArguments(String str) {
        if (!str) {
            return;
        }
        parseArguments(str.trim().split(/\s+(?=([^"']*("|')[^"']*("|'))*[^"']*$)/));
    }

    void parseArguments(String[] args) {
        if (!args) {
            return;
        }
        arguments = args.collect { return convertArgument((String) it); };
    }

    Command replaceArguments(String args) {
        Command newCommand = clone();
        if (args) {
            List<String> newArguments = (arguments == null) ? new ArrayList<String>() : arguments.toList();
            args.split(/\s+(?=([^"']*("|')[^"']*("|'))*[^"']*$)/).each {
                String token = (String) it;
                if (token =~ /^[\+-]\d.*/) {
                    int index = (((String) (token =~ /\d+/)[0]).toInteger() - 1);
                    if ((index < 0) || (index >= arguments.size())) {
                        println("ERROR:\tIndex '${index}' refers to nonexistent argument in expression '${token}'");
                        return null;
                    }

                    if (token =~ /\+(\w+)=([^\s]+)/) {
                        String value = convertArgument(token.substring(token.indexOf('=') + 1));
                        newArguments.set(index, value);
                    } else {
                        newArguments.set(index, null);
                    }
                } else {
                    newArguments.add(convertArgument(token));
                }
            }
            newArguments.removeAll(Collections.singleton(null));
            newCommand.arguments = newArguments.toArray(new String[newArguments.size()]);
        }
        return newCommand;
    }

    @Override
    String toString() {
        List<String> args = arguments.collect {
            String token = (String) it;
            if (token.contains(' ')) {
                if (token.contains('=')) {
                    String[] keyValue = token.split('=');
                    return "${keyValue[0]}=\"${keyValue[1]}\"".toString();
                } else {
                    return "\"${token}\"".toString();
                }
            } else {
                return token;
            }
        };
        return "${name} ${args.join(' ')}".trim();
    }


    private String convertArgument(String argument) {
        if (argument =~ /(['"])(?:(?=(\\?))\2.)*?\1/) {
            return argument.replaceFirst('[\'"]', '')[0..-2];
        } else {
            return argument;
        }
    }
}