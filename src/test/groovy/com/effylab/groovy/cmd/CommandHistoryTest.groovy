package com.effylab.groovy.cmd;

import org.junit.Test;
import org.junit.Before;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertNotNull;

import java.lang.reflect.Method;

class CommandHistoryTest {

    private static Command command;

    private static final String commandArguments = 
            '-flag single_value\tkey=value key="long value" key=\'long value\' \'quoted value\' "quoted value"';

    @Before
    public void init() {
        TestClass testClass = new TestClass();
        Method method = testClass.getClass().methods.find {
            it.name.equals('do_test');
        };
        command = new Command(
                name:               'test',
                briefDescription:   'Short description',
                fullDescription:    'Full description',
                arguments:          new String[0],
                method:             method,
                methodObject:       testClass
        )
    }

    @Test
    void initTest() {
        CommandHistory history = new CommandHistory();
        assertNotNull(history);
        assertEquals(0, history.getSize());
    }

    @Test
    void getMaxSizeTest() {
        CommandHistory history_1 = new CommandHistory();
        assertNotNull(history_1);
        assertEquals(100, history_1.maxSize);

        CommandHistory history_2 = new CommandHistory(10);
        assertNotNull(history_2);
        assertEquals(10, history_2.maxSize);
    }

    @Test
    void addCommandTest() {
        CommandHistory history = new CommandHistory(1);
        assertNotNull(history);
        assertEquals(1, history.maxSize);
        history.addCommand(command);
        assertEquals(1, history.getSize());
        history.addCommand(command);
        assertEquals(1, history.getSize());
    }

    @Test
    void getCommandTest() {
        CommandHistory history = new CommandHistory();
        assertNotNull(history);
        assertNull(history.getCommand(0));
        assertNull(history.getCommand(-1));
        assertNull(history.getCommand(1));
        history.addCommand(command);
        assertEquals(command, history.getCommand(0));
    }

    @Test
    void getLastCommandTest() {
        CommandHistory history = new CommandHistory(1);
        assertNotNull(history);
        assertNull(history.getLastCommand());
        history.addCommand(command);
        assertEquals(command, history.getLastCommand());
    }

    public static class TestClass {
        boolean do_test(String[] args) {
            return true;
        }
    }
}