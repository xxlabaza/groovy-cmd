package com.effylab.groovy.cmd;

import org.junit.Test;
import org.junit.Before;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertNotNull;

import java.lang.reflect.Method;

class CommandTest {

    private static Command command;

    private static final String commandArguments = 
            '-flag single_value\tkey=value key="long value" key=\'long value\' \'quoted value\' "quoted value"';

    @Before
    public void init() {
        TestClass testClass = new TestClass();
        Method method = testClass.getClass().methods.find {
            it.name.equals('do_test');
        };
        Command.methodObject = testClass;
        command = new Command(
            name:               'test',
            briefDescription:   'Short description',
            fullDescription:    'Full description',
            arguments:          new String[0],
            method:             method,
        )
    }

    @Test
    void initTest() {
        assertNotNull(command);
        assertEquals('test', command.name);
        assertEquals('Short description', command.briefDescription);
        assertEquals('Full description', command.fullDescription);
        assertEquals(new String[0], command.arguments);
    }

    @Test
    void cloneTest() {
        assertNotNull(command);

        Command clone = command.clone();
        assertNotNull(clone);
        assertNotSame(clone, command);

        assertEquals(clone.name, command.name);
        assertEquals(clone.briefDescription, command.briefDescription);
        assertEquals(clone.fullDescription, command.fullDescription);
        assertEquals(clone.arguments, command.arguments);
        assertEquals(clone.method, command.method);
        assertEquals(clone.methodObject, command.methodObject);
    }

    @Test
    void invokeTest() {
        assertNotNull(command);

        assertTrue(command.invoke());
    }

    @Test
    void parseArgumentsFromStringTest() {
        assertNotNull(command);

        Command clone_1 = command.clone();
        assertNotNull(clone_1);

        clone_1.parseArguments(commandArguments);

        assertEquals(7, clone_1.arguments.length);
        assertEquals('-flag', clone_1.arguments[0]);
        assertEquals('single_value', clone_1.arguments[1]);
        assertEquals('key=value', clone_1.arguments[2]);
        assertEquals('key=long value', clone_1.arguments[3]);
        assertEquals('key=long value', clone_1.arguments[4]);
        assertEquals('quoted value', clone_1.arguments[5]);
        assertEquals('quoted value', clone_1.arguments[6]);

        Command clone_2 = command.clone();
        assertNotNull(clone_2);

        clone_2.parseArguments('');
        assertEquals(0, clone_2.arguments.length);

        Command clone_3 = command.clone();
        assertNotNull(clone_3);

        clone_3.parseArguments((String)null);
        assertEquals(0, clone_3.arguments.length);
    }

    @Test
    void parseArgumentsFromStringArrayTest() {
        assertNotNull(command);

        Command clone_1 = command.clone();
        assertNotNull(clone_1);

        String[] args = commandArguments.split(/\s+(?=([^"']*("|')[^"']*("|'))*[^"']*$)/);
        clone_1.parseArguments(args);

        assertEquals(7, clone_1.arguments.length);
        assertEquals('-flag', clone_1.arguments[0]);
        assertEquals('single_value', clone_1.arguments[1]);
        assertEquals('key=value', clone_1.arguments[2]);
        assertEquals('key=long value', clone_1.arguments[3]);
        assertEquals('key=long value', clone_1.arguments[4]);
        assertEquals('quoted value', clone_1.arguments[5]);
        assertEquals('quoted value', clone_1.arguments[6]);

        Command clone_2 = command.clone();
        assertNotNull(clone_2);

        clone_2.parseArguments(new String[0]);
        assertEquals(0, clone_2.arguments.length);

        Command clone_3 = command.clone();
        assertNotNull(clone_3);

        clone_3.parseArguments((String[])null);
        assertEquals(0, clone_3.arguments.length);
    }

    @Test
    void replacingArgumentsTest() {
        assertNotNull(command);

        Command clone_1 = command.clone();
        assertNotNull(clone_1);
        clone_1.parseArguments(commandArguments);

        Command replcaed = clone_1.replaceArguments('+1=some +2="super_single_value" -3');
        assertNotNull(replcaed);

        assertEquals('test', replcaed.name);
        assertNotSame('-flag', replcaed.arguments[0]);
        assertEquals('some', replcaed.arguments[0]);
        assertNotSame('single_value', replcaed.arguments[1]);
        assertEquals('super_single_value', replcaed.arguments[1]);
        assertNotSame('key=value', replcaed.arguments[2]);
        assertEquals(6, replcaed.arguments.length);

        Command clone_2 = command.clone();
        assertNotNull(clone_2);

        replcaed = clone_2.replaceArguments('value_1 "some long value" \'another long value\'');
        assertEquals(3, replcaed.arguments.length);
        assertEquals('value_1', replcaed.arguments[0]);
        assertEquals('some long value', replcaed.arguments[1]);
        assertEquals('another long value', replcaed.arguments[2]);
    }

    @Test
    void toStringTest() {
        assertNotNull(command);

        Command clone_1 = command.clone();
        clone_1.parseArguments(commandArguments);
        assertNotNull(clone_1);
        assertEquals('test -flag single_value key=value key="long value" key="long value" "quoted value" "quoted value"', 
                     clone_1.toString());

        Command clone_2 = command.clone();
        assertNotNull(clone_2);
        assertEquals('test', clone_2.toString());
    }

    @Test
    void optionsParseTest() {
        TestClass2 testClass = new TestClass2();
        assertNotNull(testClass);
        Command.methodObject = testClass;

        Method method = testClass.getClass().methods.find {
            it.name.equals('do_test');
        };
        Description description = method.getAnnotation(Description);
        List<Option> optionsList = new ArrayList<>();
        for (Opt opt in description.opts()) {
            optionsList.add(new Option(opt));
        }
        Option[] options = optionsList.toArray(new Option[optionsList.size()]);
        Command optCommand = new Command(
            name:               'test',
            briefDescription:   'Short description',
            fullDescription:    'Full description',
            arguments:          new String[0],
            method:             method,
            options:            options,
            methodObject:       testClass
        );
        assertNotNull(optCommand);

        optCommand.parseArguments('-a value1 --arg2 value2 arg1 arg2');

        assertTrue(optCommand.invoke());
    }

    @Test
    void optionDefaultValueTest() {
        TestClass2 testClass = new TestClass2();
        assertNotNull(testClass);
        Command.methodObject = testClass;

        Method method = testClass.getClass().methods.find {
            it.name.equals('do_test2');
        };
        Description description = method.getAnnotation(Description);
        List<Option> optionsList = new ArrayList<>();
        for (Opt opt in description.opts()) {
            optionsList.add(new Option(opt));
        }
        Option[] options = optionsList.toArray(new Option[optionsList.size()]);
        Command optCommand = new Command(
            name:               'test',
            briefDescription:   'Short description',
            fullDescription:    'Full description',
            arguments:          new String[0],
            method:             method,
            options:            options,
            methodObject:       testClass
        );
        assertNotNull(optCommand);

        assertTrue(optCommand.invoke());
    }

    public static class TestClass {
        boolean do_test(String[] args) {
            return true;
        }
    }

    public static class TestClass2 {
        @Description(
            opts = [
                @Opt(name='a|arg1', required=true),
                @Opt(name='b|arg2')
            ]
        )
        boolean do_test(Options opts, String[] args) {
            return ((opts.get('arg1') != null) && opts.get('arg1').equals('value1')) &&
                    ((opts.get('b') != null) && opts.get('b').equals('value2')) &&
                    ((args.length == 2) && args[0].equals('arg1') && args[1].equals('arg2'));
        }

        @Description(
            opts = [
                @Opt(name='o|option', byDefault='some')
            ]
        )
        boolean do_test2(Options opts, String[] args) {
            return opts.get('o').equals(opts.get('option')) && opts.get('o').equals('some');
        }
    }
}